function parseToParams(str) {
  return str.split('&').reduce((p, c) => {
    const [key, value] = c.split('=');
    p[key] = value;
    return p;
  }, {});
}

function parseToSearch(params) {
  const str = Object.keys(params).map(k => `${k}=${params[k]}`).join('&');
  return str && ('?' + str);
}

// 添加csdn结果拦截跳转
let { search, origin, pathname } = window.location;

search = search.replace(/^\?+/, '');

let params = parseToParams(search);

let { q: wd = '' } = params;

const csdnReg = /\++\-csdn($|\++)/gi;

if (!csdnReg.test(wd)) {
  wd.replace(csdnReg, '');

  wd += '+-csdn';
  
  params.q = wd;

  window.location.href = origin + pathname + parseToSearch(params);
}
