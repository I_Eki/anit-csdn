// 为方便输入，添加input控件事件
(function() {
  const wdInput = document.querySelector('input[name=q]');

  if (!wdInput) return;

  wdInput.addEventListener('focus', e => {
    e.target.value = e.target.value.replace(/\s+\-csdn$/i, '');
  });

  wdInput.addEventListener('blur', e => {
    if (!/\s+\-csdn$/i.test(e.target.value)) {
      e.target.value += ' -csdn';
    }
  });
})();
